libobject-destroyer-perl (2.02-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libobject-destroyer-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 13 Oct 2022 22:13:51 +0100

libobject-destroyer-perl (2.02-1) unstable; urgency=medium

  * Team upload.

  [ Laurent Baillet ]
  * fix lintian spelling-error-in-description warning

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Repository.
  * Add missing build dependency on libmodule-install-perl.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.1.5, no changes needed.

  [ gregor herrmann ]
  * Import upstream version 2.02.
  * Update Upstream-Contact.
  * d/copyright: remove information about dropped files.
  * Update years of upstream copyright.
  * Update debian/upstream/metadata.
  * Update build dependencies.
  * Declare compliance with Debian Policy 4.6.1.
  * Set Rules-Requires-Root: no.
  * Remove unused lintian override.

 -- gregor herrmann <gregoa@debian.org>  Mon, 11 Jul 2022 22:13:16 +0200

libobject-destroyer-perl (2.01-1) unstable; urgency=medium

  * Team upload.

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

  [ Ansgar Burchardt ]
  * Email change: Ansgar Burchardt -> ansgar@debian.org
  * debian/control: Convert Vcs-* fields to Git.
  * New upstream release.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 4.1.4.
  * Switch to source format "3.0 (quilt)".
  * Bump debhelper compatibility level to 10.
  * debian/rules: 3 lines should be enough for everyone.
  * Drop build dependency on libtest-pod-perl.
    The test became an author test.
  * Make short description a noun phrase.
  * Update lintian override.

 -- gregor herrmann <gregoa@debian.org>  Tue, 10 Apr 2018 21:43:00 +0200

libobject-destroyer-perl (2.00-1) unstable; urgency=low

  * Initial Release (Closes: 506248)

 -- Ansgar Burchardt <ansgar@43-1.org>  Sat, 6 Dec 2008 12:35:29 +0100
